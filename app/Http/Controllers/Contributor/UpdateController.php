<?php

namespace App\Http\Controllers\Contributor;

use App\Http\Controllers\Controller;
use App\Http\Requests\Contributor\UpdateRequest;
use App\Http\Resources\ContributorResource;
use App\Models\Contributor;

class UpdateController extends Controller
{
    public function __invoke(UpdateRequest $request, Contributor $contributor)
    {
        $data = $request->validated();
        $contributor->update($data);

        return new ContributorResource($contributor);
    }
}
