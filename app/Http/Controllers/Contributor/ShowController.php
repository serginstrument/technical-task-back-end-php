<?php

namespace App\Http\Controllers\Contributor;

use App\Http\Resources\ContributorResource;
use App\Models\Contributor;

class ShowController
{
    public function __invoke(Contributor $contributor)
    {
        return new ContributorResource($contributor);
    }
}
