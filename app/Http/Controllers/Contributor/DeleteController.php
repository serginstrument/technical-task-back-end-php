<?php

namespace App\Http\Controllers\Contributor;

use App\Http\Controllers\Controller;
use App\Models\Contributor;
use Illuminate\Http\Response;

class DeleteController extends Controller
{
    public function __invoke(Contributor $contributor)
    {
        $contributor->delete();

        return response()->json(null, Response::HTTP_NO_CONTENT);
    }
}
