<?php

namespace App\Http\Controllers\Contributor;

use App\Http\Resources\ContributorResource;
use App\Models\Contributor;

class IndexController
{
    public function __invoke()
    {
        $contributors = Contributor::all();
        return ContributorResource::collection($contributors);
    }
}
