<?php

namespace App\Http\Controllers\Contributor;

use App\Http\Requests\Contributor\StoreRequest;
use App\Http\Resources\ContributorResource;
use App\Models\Contributor;

class StoreController
{
    public function __invoke(StoreRequest $request)
    {
        $data = $request->validated();
        $contributor = Contributor::create($data);

        return new ContributorResource($contributor);
    }
}
