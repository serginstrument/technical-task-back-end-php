<?php

namespace App\Http\Controllers\Collection;

use App\Models\Collection;
use Illuminate\Http\Response;

class DeleteController
{
    public function __invoke(Collection $collection)
    {
        $collection->delete();

        return response()->json(null, Response::HTTP_NO_CONTENT);
    }
}
