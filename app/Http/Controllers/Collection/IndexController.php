<?php

namespace App\Http\Controllers\Collection;

use App\Http\Resources\CollectionResource;
use App\Models\Collection;
use Illuminate\Http\Request;

class IndexController
{
    public function __invoke(Request $request)
    {
        $collections = Collection::all();
        return CollectionResource::collection($collections);

//        $maxRemainingAmount = $request->input('max_remaining_amount'); // Получаем значение из параметра запроса
//
//        $collections = Collection::whereRaw('target_amount - amount_collected <= ?', [$maxRemainingAmount])
//            ->get();
//
//        return CollectionResource::collection($collections);
    }
}
