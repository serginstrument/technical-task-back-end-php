<?php

namespace App\Http\Controllers\Collection;

use App\Http\Controllers\Controller;
use App\Http\Requests\Collection\UpdateRequest;
use App\Http\Resources\CollectionResource;
use App\Models\Collection;

class UpdateController extends Controller
{
    public function __invoke(UpdateRequest $request, Collection $collection)
    {
        $data = $request->validated();
        $collection->update($data);

        return new CollectionResource($collection);
    }
}
