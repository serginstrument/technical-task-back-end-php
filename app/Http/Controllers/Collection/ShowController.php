<?php

namespace App\Http\Controllers\Collection;

use App\Http\Resources\CollectionResource;
use App\Models\Collection;


class ShowController
{
    public function __invoke(Collection $collection)
    {
        return new CollectionResource($collection);
    }
}
