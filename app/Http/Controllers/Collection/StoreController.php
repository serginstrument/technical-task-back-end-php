<?php

namespace App\Http\Controllers\Collection;

use App\Http\Requests\Collection\StoreRequest;
use App\Http\Resources\CollectionResource;
use App\Models\Collection;

class StoreController
{
    public function __invoke(StoreRequest $request)
    {
        $data = $request->validated();
        $collection = Collection::create($data);

        return new CollectionResource($collection);
    }
}
