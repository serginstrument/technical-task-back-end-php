<?php

namespace App\Http\Controllers\Collection;

use App\Models\Collection;
use Illuminate\Http\Request;

class FilterByRemainingAmountController
{
        public function __invoke(Request $request)
    {
        $collections = Collection::with(['contributors' => function ($query) {
            $query->selectRaw('collection_id, SUM(amount) as total_amount')
                ->groupBy('collection_id');
        }])
            ->get();

        // Фільтрація зборів за залишковою сумою
        $filteredCollections = [];
        foreach ($collections as $collection) {
            foreach ($collection->contributors as $contributor) {
                $maxRemainingAmount = $collection->target_amount - $contributor->total_amount;
                if ($maxRemainingAmount > 0) {
                    $collection->maxRemainingAmount = $maxRemainingAmount;
                    $filteredCollections[] = $collection;
                    break; // Перехід до наступної колекції
                }
            }
        }

        return response()->json($filteredCollections);
    }
}
