<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contributor extends Model
{
    use HasFactory;

    protected $table = "contributors";
    public $timestamps = false;
    protected $guarded = false;

    public function collection()
    {
        return $this->belongsTo(Collection::class, 'collection_id');
    }
}
