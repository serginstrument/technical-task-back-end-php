<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Collection extends Model
{
    use HasFactory;

    protected $table = "collections";
    protected $guarded = false;

    public function contributors()
    {
        return $this->hasMany(Contributor::class, 'collection_id');
    }
}
