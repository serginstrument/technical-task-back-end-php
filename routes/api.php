<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([

    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {

    Route::post('login', [App\Http\Controllers\AuthController::class, 'login']);
    Route::post('logout', [App\Http\Controllers\AuthController::class, 'logout']);
    Route::post('refresh', [App\Http\Controllers\AuthController::class, 'refresh']);
    Route::post('me', [App\Http\Controllers\AuthController::class, 'me']);

});
Route::group([
    'middleware'=>'jwt.auth'
], function () {

Route::group([

    'prefix' => 'collections'

], function () {

    Route::get('/filter', App\Http\Controllers\Collection\FilterByRemainingAmountController::class);
    Route::get('/', App\Http\Controllers\Collection\IndexController::class);
    Route::get('/{collection}', App\Http\Controllers\Collection\ShowController::class);
    Route::post('/', App\Http\Controllers\Collection\StoreController::class);
    Route::patch('/{collection}', App\Http\Controllers\Collection\UpdateController::class);
    Route::delete('/{collection}', App\Http\Controllers\Collection\DeleteController::class);
});

Route::group([

    'prefix' => 'contributors'

], function () {

    Route::get('/', App\Http\Controllers\Contributor\IndexController::class);
    Route::get('/{contributor}', App\Http\Controllers\Contributor\ShowController::class);
    Route::post('/', App\Http\Controllers\Contributor\StoreController::class);
    Route::patch('/{contributor}', App\Http\Controllers\Contributor\UpdateController::class);
    Route::delete('/{contributor}', App\Http\Controllers\Contributor\DeleteController::class);
});

});
